import { Provider } from 'react-redux'
import Signup from './components/Signup';
import Store from './stores/Store'
import './App.css';
function App() {
  return (
    <Provider store ={Store}>
    <div>
      <Signup />
    </div>
     </Provider>
  );
}

export default App;
