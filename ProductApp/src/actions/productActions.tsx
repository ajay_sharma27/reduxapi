import axios from 'axios'
import {
    FETCH_PRODUCTS_REQUEST,
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_FAILURE
} from '../actions/productTypes'

export const fetchProducts = () => {
    return (dispatch: any) => {
        dispatch(fetchProductsRequest());
        axios
            .get('http://makeup-api.herokuapp.com/api/v1/products.json?brand=maybelline')
            .then(response => {
                const products = response.data
                dispatch(fetchProductsSuccess(products))
            })
            .catch(error => {
                const errorMsg = error.message
                dispatch(fetchProductsFailure(errorMsg))
            });
    };
}

export const fetchProductsRequest = () => {
    return {
        type: FETCH_PRODUCTS_REQUEST
    }
}

export const fetchProductsSuccess = (products: any) => {
    return {
        type: FETCH_PRODUCTS_SUCCESS,
        payload: products
    }
}

export const fetchProductsFailure = (error: any) => {
    return {
        type: FETCH_PRODUCTS_FAILURE,
        payload: error
    }
}
