import React from 'react'
import { useState } from 'react'
import GetBill from './GetBill';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

const Address = (props: any) => {
    const [address, setAddress] = useState("");
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [index, setIndex] = useState(0);
    const handleSubmit = () => {
        setIndex(index);
        setIsSubmitted(true);
    }
    return (
        <div className="container">
            {
                isSubmitted ?
                    (<div>{<Router>
                        <Switch >
                            <Route path="/get_bill" exact render={() => (
                                <div>
                                    <GetBill index_array={props.index_array} address={address} />
                                </div>
                            )
                            } />
                            <div>
                                <Redirect to="/get_bill" />
                            </div>
                        </Switch>
                    </Router>}</div>
                    ) :
                    (<div>
                        <h1 >Address Form</h1>
                        <form >
                            <div>
                                <label htmlFor="adr">Address: </label>
                                <input type="text"
                                    placeholder="542 W. 15th Street"
                                    value={address}
                                    onChange={(e) => setAddress(e.target.value)}
                                    id="adr" />
                            </div><br />
                            <div>
                                <label htmlFor="city">City: </label>
                                <input type="text"
                                    placeholder="Jaipur"
                                    id="city" />
                            </div>
                            <br />
                            <div>
                                <label htmlFor="state">State</label>
                                <input type="text" id="state" name="state" placeholder="NY" />
                            </div><br />
                            <div >
                                <label htmlFor="zip">Zip</label>
                                <input type="text" id="zip" name="zip" placeholder="10001" />
                            </div><br />
                            <div>
                                <button type="button" onClick={handleSubmit}>Bill</button>
                                <button type="reset">Reset</button>
                            </div>
                        </form><br />
                    </div>
                    )
            }

        </div>
    )
}

export default Address

