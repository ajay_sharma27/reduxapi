import { useState, useEffect } from 'react'
import Address from './Address';
import Store from '../stores/Store';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
const BuyProducts = (props: any) => {
    const [isSubmitted, setIsSubmitted] = useState<boolean>(false);
    const productData = Store.getState().productReducer.products;
    useEffect(() => {
        console.log(props.index_array)
    }, [])
    const handleSubmit = () => {

        setIsSubmitted(true);
    }
    return (
        <div>{
            isSubmitted ?
                (
                    <div>
                        {<Router>
                            <Switch >
                                <Route path="/add_address" exact render={() => (
                                    <div>

                                        <Address index_array={props.index_array} />

                                    </div>
                                )
                                } />
                                <div>
                                    <Redirect to="/add_address" />
                                </div>
                            </Switch>
                        </Router>}
                    </div>
                ) :
                (
                    <div>
                        {Array.isArray(props.index_array) && props.index_array.map((object: any) => (
                            <div>
                                <div>
                                    <h3> Product Image: </h3>  <img src={productData[object].image_link} alt="temp"></img>
                                </div> <br />
                                <div>
                                    <h3>Product Name: </h3> {productData[object].name}
                                </div> <br />

                                <div>
                                    <h3>Product Price: </h3> {productData[object].price}
                                </div> <br />

                                <div>
                                    <h3>Product Description: </h3> {productData[object].description}
                                </div> <br />
                            </div>
                        ))}
                        <div>
                            <button type="button" onClick={handleSubmit}>Address</button>
                        </div> <br />
                    </div>
                )
        }
        </div>
    )
}
export default BuyProducts
