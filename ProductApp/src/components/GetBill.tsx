import Store from '../stores/Store';
const GetBill = (props: any) => {
    const productData = Store.getState().productReducer.products;
    const user_names = Store.getState().userReducer.username;
    return (
        <div>
            <h2>Bill</h2> <hr />
            <div>
                <h5>Username:</h5> {user_names[1]}
            </div>
            <div>
                <h5>Address: </h5> {props.address}
            </div>
            <div>
            {Array.isArray(props.index_array) && props.index_array.map((object:any) => (
                <div >
                    <h5>Product Purchased:</h5> {productData[object].name}
                    <h5>Product Price:</h5> {productData[object].price}
                </div>
            ))}
                </div>
        </div>
    )
}
export default GetBill
