// import { useEffect, useState } from 'react'
// import { connect } from 'react-redux'
// import { fetchProducts } from "../actions/productActions"
// import {
//     BrowserRouter as Router,
//     Switch,
//     Route,
//     Redirect
// } from "react-router-dom";
// import BuyProducts from './BuyProducts';
// import { useDispatch } from "react-redux"
// import { fetchIndexDetails } from '../actions/indexActions';
// function Home({ productData, fetchProducts }: any) {
//     const [isSubmitted, setIsSubmitted] = useState(false);
//     const [index, setIndex] = useState(['']);
//     const [item, setItem] = useState("");
//     const dispatch = useDispatch();
//     useEffect(() => {
//         fetchProducts()
//     }, [])
// const AddProduct =(index:any) =>{

//     // setIndex(index);
//     setIndex([...index, item]);
// }
//     const handleSubmit = () => {

//         setIsSubmitted(true);

//     }
//     useEffect(() => {
//         console.log(index);
//         dispatch(fetchIndexDetails(index));
//     }, [dispatch, index]);
//     console.log(index)
//     return (
//     <div className="cont">
//         {productData.loading ? (
//             <h2>Loading</h2>
//         ) : productData.error ? (
//             <h2>{productData.error}</h2>
//         ) : isSubmitted ?
//             (
//                 <div>
//                     {<Router>
//                         <Switch >
//                             <Route path="/buy_product" exact render={() => (
//                                 <div>
//                                     <BuyProducts/>
//                                     {/* <BuyProducts index={index} image={productData[index].image_link} name= {productData[index].name} price={productData[index].price} description={productData[index].description} /> */}
//                                 </div>
//                                 )
//                             } />
//                             <div>
//                                 <Redirect to="/buy_product" />
//                             </div>
//                         </Switch>
//                     </Router>}
//                 </div>
//             ) :
//             (
//                 <div>
//                     <h2>Products List</h2>
//                     <div > 
//                         {Array.isArray(productData) && productData.map((object:any, index:any)=> (
//                             <div key={object.uid}>
//                                 <img src={object.image_link} alt = "temp"></img> <br /> 
//                                 {object.name} <br /> 
//                                 {/* {object.price} <br />
//                                 {object.description} <br /> */}
//                                 <button type="button" onClick={() => AddProduct(index)}>Add</button> <hr />
//                             </div>
                            
//                         ))}
//                         <button type="button" onClick={() => handleSubmit()}>Buy</button>
                        
//                     </div>
//                 </div>
//             )
//         }
//     </div>
//     )
// }

// const mapStateToProps = (state: any) => {
//     return {
//         productData: state.productReducer.products
//     }
// }

// const mapDispatchToProps = (dispatch: any,) => {
//     return {
//         fetchProducts: () => dispatch(fetchProducts())
//     }
// }

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(Home)

import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { fetchProducts } from "../actions/productActions"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import BuyProducts from './BuyProducts';

function Home({ productData, fetchProducts }: any) {
    const [isSubmitted, setIsSubmitted] = useState<boolean>(false);
    const [index, setIndex] = useState(0);
    
    const [index_array, setIndex_array] = useState<Array<Number>>([]);
    // const [item, setIndex] = useState<number>(0);
    useEffect(() => {
        fetchProducts()
    }, [])
    const addProduct = (ind:number) => {
        setIndex_array([...index_array, ind]);
    }
    const handleSubmit = () => {

        setIsSubmitted(true);

    }
   
    return (
    <div className="cont">
        {productData.loading ? (
            <h2>Loading</h2>
        ) : productData.error ? (
            <h2>{productData.error}</h2>
        ) : isSubmitted ?
            (
                <div>
                    {<Router>
                        <Switch >
                            <Route path="/buy_product" exact render={() => (
                                <div>
                                    <BuyProducts index={index} index_array = {index_array} image={productData[index].image_link} name= {productData[index].name} price={productData[index].price} description={productData[index].description} />
                                </div>
                                )
                            } />
                            <div>
                                <Redirect to="/buy_product" />
                            </div>
                        </Switch>
                    </Router>}
                </div>
            ) :
            (
                <div>
                    <h2>Products List</h2>
                    <div > 
                        {Array.isArray(productData) && productData.map((object, index)=> (
                            <div key={object.uid}>
                                <img src={object.image_link} alt = "temp"></img> <br /> 
                                {object.name} <br /> 
                                {/* {object.price} <br />
                                {object.description} <br /> */}
                                <button type="button" onClick={() => addProduct(index)}>Add</button> <hr />
                            </div>
                        ))}
                        <button type="button" onClick={() => handleSubmit()}>Buy</button> <hr />
                        
                    </div>
                </div>
            )
        }
    </div>
    )
}

const mapStateToProps = (state: any) => {
    return {
        productData: state.productReducer.products
    }
}

const mapDispatchToProps = (dispatch: any,) => {
    return {
        fetchProducts: () => dispatch(fetchProducts())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)



