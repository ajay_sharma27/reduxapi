import React from 'react'
import Home from './Home'
import Signup from './Signup';
import { useState } from 'react'
import Store from '../stores/Store';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    Link
} from "react-router-dom";
function Login() {
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<any>("");
    const [goodToGo, setGoodToGo] = useState<boolean>(false);
    const HandlerUsernameChange = () => {
        const user_names = Store.getState().userReducer.username
        if (user_names.includes(username)) {
            setGoodToGo(true);
        } else {
            setGoodToGo(false);
        }
    }
    return (
        <div className="container">
            {
                goodToGo ?
                    <Router>
                        <Switch >
                            <Route path="/registration" exact component={Signup} />
                            <Route path="/home" exact component={Home} />
                            <div>
                                <Redirect to="/home" />
                            </div>
                        </Switch>
                    </Router>
                    :
                    <div>
                        <h1 >Login</h1>
                        <form >
                            <div><label htmlFor="username">Username: </label>
                                <input type="username"
                                    placeholder="Name"
                                    value={username}
                                    onChange={(e) => setUsername(e.target.value)}
                                    id="username" />
                            </div><br />
                            <div>
                                <label htmlFor="password">Password: </label>
                                <input type="password"
                                    placeholder="Password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    id="password" />
                            </div>
                            <br />
                            <div>
                                <button type="button" onClick={HandlerUsernameChange} >Submit</button>
                                <button type="reset">Reset</button>
                            </div>
                            <div>
                                <Router>
                                    <Route path="/registration" exact component={Signup} />
                                    <p>If not Registered yet! <button type="button">{<Link to="/registration">Signup</Link>}</button></p>
                                </Router>
                            </div>
                        </form><br />
                    </div>
            }
        </div>
    )
}

export default (Login)