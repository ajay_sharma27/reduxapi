import Login from './Login'
import { fetchUserDetails } from "../actions"
import { useState, useEffect } from 'react'
import { useDispatch } from "react-redux"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
function Signup() {
    const [usernames, setUsernames] = useState(['']);
    const [password, setPassword] = useState("");
    const [currUser, setCurrUser] = useState("");
    const [isSubmitted, setIsSubmitted] = useState(false);
    const dispatch = useDispatch();
    const handleSubmit = () => {
        setUsernames([...usernames, currUser]);
        setIsSubmitted(true);
    }
    useEffect(() => {
        console.log(usernames);
        dispatch(fetchUserDetails(usernames));
    }, [dispatch, usernames]);
    console.log(usernames)
    return (
        <div className="container">
            {isSubmitted ?
                <Router>
                    <Switch >
                        <Route path="/login" exact component={Login} />
                        <div>
                            <Redirect to="/login" />
                        </div>
                    </Switch>
                </Router> :
                <div>
                    <h1 >Signup</h1>
                    <form >
                        <div>
                            <label htmlFor="username">Username: </label>
                            <input type="username"
                                placeholder="Name"
                                value={currUser}
                                onChange={(e) => setCurrUser(e.target.value)}
                                id="username" />
                        </div><br />
                        <div>
                            <label htmlFor="password">Password: </label>
                            <input type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                id="password" />
                        </div>
                        <br />
                        <div>
                            <button type="button" onClick={handleSubmit}>Submit</button>
                            <button type="reset">Reset</button>
                        </div>
                    </form><br />
                </div>
            }
        </div>
    )
}
export default Signup
