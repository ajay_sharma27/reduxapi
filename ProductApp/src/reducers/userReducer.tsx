import {USER_DETAILS} from "../actions/userTypes"
const initialState = {
    username: [""]
}

const userReducer = (state = initialState, action:any) =>{ 
    console.log(action.payload);
    switch(action.type){
        case USER_DETAILS: return{
            username: action.payload
        }
        default: return state
    }
}
export {USER_DETAILS}
export default  userReducer;